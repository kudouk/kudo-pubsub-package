package pubsub

import (
	"context"
	"fmt"

	"cloud.google.com/go/pubsub"
)

func createTopic(ctx context.Context, client *pubsub.Client, topicName string) (*pubsub.Topic, error) {
	fmt.Println("Create New Topic:", topicName)
	topic := client.Topic(topicName)
	exists, err := topic.Exists(ctx)
	if err != nil {
		return nil, err
	}
	if !exists {
		if _, err := client.CreateTopic(ctx, topicName); err != nil {
			return nil, err
		}
	}
	return topic, nil
}
