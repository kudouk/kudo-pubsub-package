package pubsub

type ServiceType string

const (
	PUBSUB_USER                      ServiceType = "PUBSUB_USER"
	PUBSUB_NOTIFICATION              ServiceType = "PUBSUB_NOTIFICATION"
	PUBSUB_CART                      ServiceType = "PUBSUB_CART"
	PUBSUB_KUDO                      ServiceType = "PUBSUB_KUDO"
	PUBSUB_SPECIFICATION             ServiceType = "PUBSUB_SPECIFICATION"
	PUBSUB_PRODUCT                   ServiceType = "PUBSUB_PRODUCT"
	PUBSUB_SUPPLIER                  ServiceType = "PUBSUB_SUPPLIER"
	PUBSUB_ADMIN                     ServiceType = "PUBSUB_ADMIN"
	PUBSUB_MEDIA                     ServiceType = "PUBSUB_MEDIA"
	PUBSUB_ORDER                     ServiceType = "PUBSUB_ORDER"
	PUBSUB_CRONJOBS                  ServiceType = "PUBSUB_CRONJOBS"
	PUBSUB_AUTH                      ServiceType = "PUBSUB_AUTH"
	PUBSUB_WEBSOCKET                 ServiceType = "PUBSUB_WEBSOCKET"
	PUBSUB_PRICINGSTORE              ServiceType = "PUBSUB_PRICINGSTORE"
	PUBSUB_PRICINGSTORE_BATCH_CHANGE ServiceType = "PUBSUB_PRICINGSTORE_BATCH_CHANGE"
	PUBSUB_PRICING_SPEC_MATCH        ServiceType = "PUBSUB_PRICING_SPEC_MATCH"
	PUBSUB_ARTWORKER_API             ServiceType = "PUBSUB_ARTWORKER_API"
	PUBSUB_ARTWORKING                ServiceType = "PUBSUB_ARTWORKING"
	PUBSUB_ARTWORKER                 ServiceType = "PUBSUB_ARTWORKER"
)

type Handler interface {
	PubsubReceive(st ServiceType, mt MessageEventType, d []byte) error
}
