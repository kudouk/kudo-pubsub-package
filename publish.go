package pubsub

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"

	"cloud.google.com/go/pubsub"
)

type Publisher struct {
	topic *pubsub.Topic
}

func (c *Client) NewPublisher(st ServiceType, ctx context.Context) *Publisher {
	topic, err := createTopic(ctx, c.Client, os.Getenv(fmt.Sprintf("%v_TOPIC", st)))
	if err != nil {
		log.Fatalf("fatal pubsub.CreateTopic %v", err)
	}

	return &Publisher{
		topic: topic,
	}
}

// Close - use: defer p.Close(), closes all go routines & publishes remaining messages
// before closing
func (p *Publisher) Close() {
	p.topic.Stop()
}

func (p *Publisher) Publish(mt MessageEventType, data interface{}) error {
	ctx := context.Background()

	event := MessageEvent{
		EventType: mt,
		Data:      data,
	}

	n, err := json.Marshal(event)
	if err != nil {
		return err
	}

	_, err = p.topic.Publish(ctx, &pubsub.Message{Data: n}).Get(ctx)
	return err
}
