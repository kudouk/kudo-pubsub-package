package pubsub

type MessageEventType string

const (
	// supplier
	SUPPLIER_CREATED        MessageEventType = "supplier-created"
	SUPPLIER_LOGIN          MessageEventType = "supplier-login"
	SUPPLIER_RESET_PASSWORD MessageEventType = "supplier-reset-password"
	SUPPLIER_APPROVED       MessageEventType = "supplier-approved"
	SUPPLIER_EMAIL_VERIFY   MessageEventType = "supplier-verify-email"
	SUPPLIER_ADMIN_CREATED  MessageEventType = "supplier-admin-created"

	// specification
	TENDER_CREATED         MessageEventType = "tender-created"
	TENDER_EXPIRING_SOON   MessageEventType = "tender-expiring-soon"
	BESPOKE_JOB_CREATED    MessageEventType = "bespoke-job-created"
	BESPOKE_JOB_SUBMITTED  MessageEventType = "bespoke-job-submitted"
	BESPOKE_TENDER_CREATED MessageEventType = "bespoke-tender-created"
	// customer
	APPUSER_LOGIN           MessageEventType = "appuser-login"
	APPUSER_CREATED         MessageEventType = "appuser-created"
	APPUSER_RESET_PASSWORD  MessageEventType = "appuser-reset-password"
	APPUSER_ADDRESS_UPDATED MessageEventType = "appuser-address-updated"
	APPUSER_ADDRESS_DELETED MessageEventType = "appuser-address-deleted"
	// order
	ORDER_CREATED                MessageEventType = "order-created"
	ORDER_ITEM_IN_PRODUCTION     MessageEventType = "order-item-in-production"
	ORDER_ITEM_ARTWORKING_EVENTS MessageEventType = "order-item-artworking-events"
	ORDER_ITEM_DELIVERY_EVENT    MessageEventType = "order-item-delivery-event"
	ORDER_ITEM_REFUND_ISSUED     MessageEventType = "order-item-refund-issued"
	// cronjob
	CRONJOB_NOTIFY_SOON_EXPIRE_TENDERS MessageEventType = "notify-soon-expire-tenders"
	CRONJOB_CLEANUP_EXPIRED_TENDERS    MessageEventType = "cleanup-expired-tenders"
	CRONJOB_ORDER_ITEM_COMPLETE        MessageEventType = "complete-order-item-after-x-days"

	PRICINGSTORE_PROCESS MessageEventType = "pricingstore-process"
	PRICINGSTORE_ANALYSE MessageEventType = "pricingstore-analyse"

	PRICINGSTORE_PROCESS_STARTED  MessageEventType = "pricingstore-process-started"
	PRICINGSTORE_PROCESS_PROGRESS MessageEventType = "pricingstore-process-progress"
	PRICINGSTORE_PROCESS_ERROR    MessageEventType = "pricingstore-process-error"
	PRICINGSTORE_PROCESS_ENDED    MessageEventType = "pricingstore-process-ended"

	PRICINGSTORE_BATCH_UPDATED    MessageEventType = "pricingstore-batch-updated"
	PRICINGSTORE_CHAIN_SPEC_MATCH MessageEventType = "pricingstore-chain-spec-match"

	SPEC_PRICING_RECALCULATE_BATCH MessageEventType = "spec-pricing-recalculate-batch"
	SPEC_PRICING_RECALCULATE       MessageEventType = "spec-pricing-recalculate"

	CREATE_SPECS_FROM_CHAINS MessageEventType = "create-spec-from-chain"

	CSV_UPLOAD_EXPIRED       MessageEventType = "csv-upload-expired"
	CSV_UPLOAD_EXPIRING_SOON MessageEventType = "csv-upload-expiring-soon"

	ARTWORKER_EVENT_AUTH0_USER_CREATED MessageEventType = "artworker-auth0-user-created"

	ARTWORK_REQUEST_CREATED                   MessageEventType = "artwork-request-created"
	ARTWORK_REQUEST_CUSTOMER_UPLOADED         MessageEventType = "artwork-request-customer-uploaded"
	ARTWORK_REQUEST_CUSTOMER_RESPONDED        MessageEventType = "artwork-request-customer-responded"
	ARTWORK_REQUEST_CUSTOMER_ACCEPTED_CHANGES MessageEventType = "artwork-request-customer-accepted-changes"
	ARTWORK_REQUEST_SUPPLIER_APPROVED         MessageEventType = "artwork-request-supplier-approved"
	ARTWORK_REQUEST_SUPPLIER_REJECTED         MessageEventType = "artwork-request-supplier-rejected"
	ARTWORK_REQUEST_PROOF_CREATED             MessageEventType = "artwork-request-proof-created"
	ARTWORK_REQUEST_CUSTOMER_PROOF_REJECTED   MessageEventType = "artwork-request-customer-proof-rejected"

	WS_PUBLISH MessageEventType = "ws-publish"
)
