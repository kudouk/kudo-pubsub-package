package pubsub

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"

	"cloud.google.com/go/pubsub"
)

type MessageEvent struct {
	EventType MessageEventType
	Data      interface{}
}

func createSubscription(ctx context.Context, client *pubsub.Client, topic *pubsub.Topic, subName string) (*pubsub.Subscription, error) {
	subscription := client.Subscription(subName)
	exists, err := subscription.Exists(ctx)
	if err != nil {
		return nil, err
	}
	if !exists {
		if _, err = client.CreateSubscription(ctx, subName, pubsub.SubscriptionConfig{Topic: topic}); err != nil {
			return nil, err
		}
	}
	return subscription, nil
}

func Subscribe(client *pubsub.Client, i Handler, subscribeTo ServiceType, subscribeFrom string, ctx context.Context) {
	fmt.Println("Subscribe")

	topic, err := createTopic(ctx, client, os.Getenv(fmt.Sprintf("%v_TOPIC", subscribeTo)))
	if err != nil {
		log.Fatalf("fatal pubsub.CreateTopic %v", err)
	}
	sub, err := createSubscription(ctx, client, topic, fmt.Sprintf("%v-%v", os.Getenv(fmt.Sprintf("%v_SUBSCRIPTION", subscribeTo)), subscribeFrom))
	if err != nil {
		log.Fatalf("fatal pubsub.CreateSubscription %v", err)
	}

	err = sub.Receive(ctx, func(ctx context.Context, msg *pubsub.Message) {
		msg.Ack()
		fmt.Println("MessageEvent.Recieved", msg.DeliveryAttempt, msg.ID)
		e := MessageEvent{}

		if err := json.Unmarshal(msg.Data, &e); err != nil {
			fmt.Println("MessageEvent.Recieved Err", err)
			return
		}

		err := i.PubsubReceive(subscribeTo, e.EventType, msg.Data)
		if err != nil {
			log.Println("MessageEventErr", err)
		}

		fmt.Println("MessageEvent.Recieved.DONE")
	})
	if err != nil {
		fmt.Println("ERRRORRRRR", err)
		// log.Fatal(err)
	}

}

type Message struct {
	SubscribeTo ServiceType
	EventType   MessageEventType
	Data        []byte
}

func (c *Client) SubscribeToChan(ctx context.Context, subscribeTo ServiceType, subscribeFrom string, result chan<- *Message) {
	fmt.Println("Subscribe")
	defer close(result)

	// var (
	// 	countMu sync.Mutex
	// 	count   int
	// )

	topic, err := createTopic(ctx, c.Client, os.Getenv(fmt.Sprintf("%v_TOPIC", subscribeTo)))
	if err != nil {
		log.Fatalf("fatal pubsub.CreateTopic %v", err)
	}
	sub, err := createSubscription(ctx, c.Client, topic, fmt.Sprintf("%v-%v", os.Getenv(fmt.Sprintf("%v_SUBSCRIPTION", subscribeTo)), subscribeFrom))
	if err != nil {
		log.Fatalf("fatal pubsub.CreateSubscription %v", err)
	}

	err = sub.Receive(ctx, func(ctx context.Context, msg *pubsub.Message) {
		fmt.Println("MessageEvent.Recieved")
		e := MessageEvent{}

		if err := json.Unmarshal(msg.Data, &e); err != nil {
			fmt.Println("MessageEvent.Recieved Err", err)
			msg.Ack()
			return
		}

		result <- &Message{
			SubscribeTo: subscribeTo,
			EventType:   e.EventType,
			Data:        msg.Data,
		}

		// countMu.Lock()
		// count++
		// countMu.Unlock()

		msg.Ack()
	})
	if err != nil {
		log.Fatal(err)
	}

}
