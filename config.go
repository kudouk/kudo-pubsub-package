package pubsub

import (
	"context"

	"cloud.google.com/go/pubsub"
)

type Client struct {
	Client *pubsub.Client
}

func ConfigurePubsub(ctx context.Context, projectID string) (*Client, error) {
	client, err := pubsub.NewClient(ctx, projectID)
	if err != nil {
		return &Client{}, err
	}

	return &Client{
		Client: client,
	}, nil
}

// go pubsub.Subscribe(pubsub.UserSub, service)
